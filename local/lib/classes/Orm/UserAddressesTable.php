<?php

namespace TestProject\Classes\Orm;

use Bitrix\Main\Entity\Event;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\BooleanField;
use CBitrixComponent;
use ShowUserAddresses;

class UserAddressesTable extends DataManager
{
    /**
     * @return string
     */
    public static function getTableName(): string
    {
        return 'tp_user_addresses';
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    public static function getMap(): array
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => 'ID записи'
                ]
            ),
            new IntegerField('USER_ID', ['title' => 'ID пользователя']),
            new StringField('ADDRESS', ['title' => 'Адрес пользователя']),
            new BooleanField('ACTIVE', ['values' => ['N', 'Y']]),
        ];
    }

    /**
     * @param Event $event
     */
    public static function OnAfterAdd(Event $event): void
    {
        CBitrixComponent::includeComponentClass('test-project:show.user.addresses');
        ShowUserAddresses::clearTaggedCache();
    }

    /**
     * @param Event $event
     */
    public static function OnAfterUpdate(Event $event): void
    {
        CBitrixComponent::includeComponentClass('test-project:show.user.addresses');
        ShowUserAddresses::clearTaggedCache();
    }

    /**
     * @param Event $event
     */
    public static function OnAfterDelete(Event $event): void
    {
        CBitrixComponent::includeComponentClass('test-project:show.user.addresses');
        ShowUserAddresses::clearTaggedCache();
    }
}
