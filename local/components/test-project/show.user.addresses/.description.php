<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = [
    'NAME' => Loc::getMessage('COMPONENT_NAME'),
    'DESCRIPTION' => Loc::getMessage('COMPONENT_DESCRIPTION'),
    'PATH' => [
        'ID' => 'custom',
        'NAME' => Loc::getMessage('COMPONENT_PATH_NAME'),
    ],
    'CACHE_PATH' => 'Y',
    'COMPLEX' => 'N'
];
