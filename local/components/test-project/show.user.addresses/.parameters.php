<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = [
    'PARAMETERS' => [
        'ACTIVE_ONLY' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('ACTIVE_ONLY_NAME'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N'
        ],
    ],
];
