<?php

use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Engine\CurrentUser;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use TestProject\Classes\Orm\UserAddressesTable;

class ShowUserAddresses extends CBitrixComponent
{
    /**
     * @var int
     */
    protected int $userId = 0;

    protected const CACHE_TIME = 3600;
    protected const CACHE_PATH = 'user_addresses_dir';
    protected const CACHE_ID = 'user_addresses_cache';

    /**
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams): array
    {
        $arParams['ACTIVE_ONLY'] = $arParams['ACTIVE_ONLY'] == 'Y' ? 'Y' : 'N';
        return $arParams;
    }

    /**
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function executeComponent(): void
    {
        $this->setUserId();
        if (!$this->checkPermissions()) {
            return;
        }
        $this->arResult['GRID_ID'] = $this->getComponentId();
        $this->arResult['ITEMS'] = $this->getItems();
        $this->includeComponentTemplate();
    }

    private function setUserId(): void
    {
        $this->userId = (int) CurrentUser::get()->getId();
    }

    /**
     * @return bool
     */
    protected function checkPermissions(): bool
    {
        return $this->userId > 0;
    }

    /**
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    protected function getItems(): array
    {
        $cache = Application::getInstance()->getCache();
        $taggedCache = Application::getInstance()->getTaggedCache();
        if ($cache->initCache(static::CACHE_TIME, static::CACHE_ID, static::CACHE_PATH)) {
            $result = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $taggedCache->startTagCache(static::CACHE_PATH);
            $taggedCache->registerTag(static::CACHE_ID);

            $result = $this->fetchItems();

            $taggedCache->endTagCache();
            $cache->endDataCache($result);
        }
        return $result;
    }

    /**
     * @return array
     */
    protected function getParamsForFetch(): array
    {
        $arFilter = ['=USER_ID' => $this->userId];
        if ($this->arParams['ACTIVE_ONLY'] == 'Y') {
            $arFilter['=ACTIVE'] = 'Y';
        }
        return [
            'filter' => $arFilter,
            'select' => ['ID', 'ADDRESS', 'ACTIVE'],
            'order' => ['ID' => 'asc'],
            'cache' => [
                'ttl' => static::CACHE_TIME
            ]
        ];
    }

    /**
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    protected function fetchItems(): array
    {
        $dbResult = UserAddressesTable::getList(static::getParamsForFetch());
        $arResult = [];
        while($arItem = $dbResult->fetch()) {
            $arResult[] = [
                'data' => [
                    'ID' => $arItem['ID'],
                    'ADDRESS' => $arItem['ADDRESS'],
                    'ACTIVE' => $arItem['ACTIVE']
                ]
            ];
        }
        return $arResult;
    }

    /**
     * @return string
     */
    protected function getComponentId(): string
    {
        return 'component_' . mb_substr(md5(serialize($this->arParams)), 10) . $this->randString();
    }

    public static function clearTaggedCache(): void
    {
        $taggedCache = Application::getInstance()->getTaggedCache();
        $taggedCache->clearByTag(static::CACHE_ID);
    }
}
