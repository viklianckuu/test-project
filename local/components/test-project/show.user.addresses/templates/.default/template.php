<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->IncludeComponent(
    'bitrix:main.ui.grid',
    '',
    [
        'GRID_ID' => $arResult['GRID_ID'],
        'COLUMNS' => [
            [
                'id' => 'ID',
                'name' => 'ID',
                'default' => true
            ],
            [
                'id' => 'ADDRESS',
                'name' => Loc::getMessage('COLUMN_ADDRESS'),
                'default' => true
            ],
            [
                'id' => 'ACTIVE',
                'name' => Loc::getMessage('COLUMN_ACTIVE'),
                'default' => true
            ],
        ],
        'ROWS' => $arResult['ITEMS'],
        'SHOW_ROW_CHECKBOXES' => false,
        'SHOW_NAVIGATION_PANEL' => false,
        'ALLOW_SORT' => false,
    ]
);
